#!/bin/sh
#set debug to be '1' (debug==ON)
debug="0"
((debug)) || exec 2>/dev/null

function getDnsNatIP() {
	ssh_cmd="sshpass -p $PASSWORD ssh -o StrictHostKeyChecking=no -l $USER $1"
	hostname=`$ssh_cmd list sys global-settings hostname | grep hostname | awk '{print $2}'`
	vlaName=`$ssh_cmd list net vlan one-line | grep 231| awk '{print $3}'`
	vsList=`$ssh_cmd list ltm virtual one-line | grep $vlaName | awk '{print $3}'`

	for vs in $vsList;do
		src_addr_translate_pool=`$ssh_cmd list ltm virtual $vs source-address-translation { pool } | grep pool | awk '{print $2}'`
		src_addr_translate_type=`$ssh_cmd list ltm virtual $vs source-address-translation { type } | grep type | awk '{print $2}'`
		if [[ $src_addr_translate_type == "lsn" ]];then
			NAT_IP=`$ssh_cmd list ltm lsn-pool $src_addr_translate_pool members | grep / | tr -d [:blank:] | tr "\n" "," | sed 's/,$//'`
		elif [[ $src_addr_translate_type == "snat" ]];then
			NAT_IP=`$ssh_cmd list ltm snatpool $src_addr_translate_pool members | grep / | tr -d [:blank:] | tr "\n" "," | sed 's/,$//'`
		else
			NAT_IP=""
		fi
		#echo -e "$hostname;$vs with NAT IP as follow:\n$NAT_IP"
		echo -e "$hostname,$vs,$NAT_IP"
	done
}

#F5 IP
#ChangeThis
aa="10.1.1.x"
ab="10.1.1.y"


echo "HOSTNAME,VS_NAME,NAT_IP" > ./collectDnsNatIP.csv
declare -a arrHosts=($aa $ab)
for host in ${arrHosts[@]};do
	getDnsNatIP $host >> ./collectDnsNatIP.csv
done
