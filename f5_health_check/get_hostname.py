#!/usr/bin/python
from subprocess import Popen, PIPE

#ChangeThis
hosts = ("10.x.x.1", "10.x.x.2", "10.x.x.3")

retry_counter = 3
hostnames = dict()
for host in hosts:
	retry = 0
	while retry < retry_counter:
		p = Popen("snmpget -v2c -c F5_PDN " + host + " .1.3.6.1.4.1.3375.2.1.6.2.0", shell=True, stdout=PIPE, stderr=PIPE)
		stdout = p.communicate()[0]
		if stdout:
			hostnames[host] = stdout.split("STRING:")[1].strip().strip('"')
			break
		else:
			retry += 1

for host in hosts:
	print "'" + host + "'" + " : " + "'" + hostnames.get(host, host) + "'"