#!/usr/bin/python
#mailto: wahyu.herdyanto@dimensiondata.com
#last update: 14-August-2017

import time
import datetime
import re
import threading
import sys
import socket
import operator
from Queue import Queue
from subprocess import Popen
from subprocess import PIPE
from openpyxl import Workbook
from openpyxl.styles import PatternFill, Border, Side, Alignment, Protection, Font



########################################################################
isAlert = 1
output_csv = "F5_healthcheck.csv"
output_xlsx = "F5_healthcheck.xlsx"

dd_walk = "snmpwalk -v2c -c F5_PDN "
telegram_cli = "10.251.16.138"

#this interval is for traffic bps / connection collection, in seconds
F5_interval = 10
retry_counter = 3

dictEntry = dict()
headers = ("Hostname", "HA Status", "Sync Status", "Blades Status", "Uptime", "Disk Space", "Chassis Temperature", "CRC Error", "CPU", "Memory", "Throughput", "Connections", "TCP Reset")

hosts = {'10.251.15.107' : 'TBS-PB300-VIP4800-1-F5.telkomsel.co.id',\
 '10.251.15.108' : 'TBS-PB300-VIP4800-2-F5.telkomsel.co.id',\
 '10.251.15.77' : 'TBS-VIP4800-PB300-3-F5.telkomsel.co.id',\
 '10.251.15.93' : 'TBS-VIP4800-PB300-4-F5.telkomsel.co.id',\
 '10.251.15.125' : 'TBS-VIP4800-PB300-5-F5.telkomsel.co.id',\
 '10.251.165.107' : 'BRN-PB300-VIP4800-1-F5.telkomsel.co.id',\
 '10.251.165.108' : 'BRN-PB300-VIP4800-2-F5.telkomsel.co.id',\
 '10.251.165.77' : 'BRN-VIP4800-PB300-3-F5.telkomsel.co.id',\
 '10.251.165.93' : 'BRN-VIP4800-PB300-4-F5.telkomsel.co.id',\
 '10.251.165.109' : 'BRN-VIP4800-PB300-5-F5.telkomsel.co.id',\
 '10.35.213.70' : 'PKB-PB200-1-F5.telkomsel.co.id',\
 '10.35.213.71' : 'PKB-PB200-2-F5.telkomsel.co.id',\
 '10.35.213.80' : 'PKB2-PB200-1-F5.telkomsel.co.id',\
 '10.35.213.81' : 'PKB2-PB200-2-F5.telkomsel.co.id',\
 '10.35.213.96' : 'PKB-VIP2200-2250-3-F5.telkomsel.co.id',\
 '10.35.213.97' : 'PKB-VIP2200-2250-4-F5.telkomsel.co.id',\
 '10.35.213.98' : 'PKB-VIP2200-2250-5-F5.telkomsel.co.id',\
 '10.35.213.99' : 'PKB-VIP2200-B2250-6-F5.telkomsel.co.id',\
 '10.35.213.100' : 'PKB-VIP2200-B2250-7-F5.telkomsel.co.id',\
 '10.35.213.101' : 'PKB-VIP2200-B2250-8-F5.telkomsel.co.id',\
 '10.35.185.20' : 'KNG-VIP4800-PB300-1-F5.telkomsel.co.id',\
 '10.35.185.25' : 'KNG-VIP4800-PB300-2-F5.telkomsel.co.id',\
 '10.35.185.6' : 'KNG-VIP2200-2250-3-F5.telkomsel.co.id',\
 '10.35.185.164' : 'KNG-VIP2200-2250-4-F5.telkomsel.co.id',\
 '10.35.185.8' : 'KNG-VIP2200-2250-5-F5.telkomsel.co.id',\
 '10.35.185.176' : 'KNG-VIP2200-2250-6-F5.telkomsel.co.id',\
 '10.60.100.7' : 'SBY-PB200-1-F5.telkomsel.co.id',\
 '10.60.100.8' : 'SBY-PB200-2-F5.telkomsel.co.id',\
 '10.60.100.43' : 'SBY-VIP2200-B2250-3-F5.telkomsel.co.id',\
 '10.60.100.46' : 'SBY-VIP2200-B2250-4-F5.telkomsel.co.id',\
 '10.60.100.49' : 'SBY-VIP2200-B2250-5-F5.telkomsel.co.id',\
 '10.60.100.36' : 'SBY-VIP2200-B2250-6-F5.telkomsel.co.id',\
 '10.60.100.37' : 'SBY-VIP2200-B2250-7-F5.telkomsel.co.id',\
 '10.60.100.38' : 'SBY-VIP2200-B2250-8-F5.telkomsel.co.id',\
 '10.77.113.46' : 'HRM-VIP2200-2250-1-F5.telkomsel.co.id',\
 '10.77.113.47' : 'HRM-VIP2200-2250-2-F5.telkomsel.co.id',\
 '10.77.113.48' : 'HRM-VIP2200-2250-3-F5.telkomsel.co.id',\
 '10.77.113.49' : 'HRM-VIP2200-2250-4-F5.telkomsel.co.id',\
 '10.77.113.50' : 'HRM-VIP2200-2250-5-F5.telkomsel.co.id',\
 '10.77.113.51' : 'HRM-VIP2200-2250-6-F5.telkomsel.co.id',\
 '10.77.113.52' : 'HRM-VIP2200-2250-7-F5.telkomsel.co.id',\
 '10.97.216.6' : 'UPD-VIP4800-PB300-1-F5.telkomsel.co.id',\
 '10.97.216.7' : 'UPD-VIP4800-PB300-2-F5.telkomsel.co.id',\
 '10.97.216.164' : 'UPD-VIP2200-2250-3-F5.telkomsel.co.id',\
 '10.97.216.165' : 'UPD-VIP2200-B2250-4-F5.telkomsel.co.id',\
 '10.97.216.166' : 'UPD-VIP2200-B2250-5-F5.telkomsel.co.id',\
 '10.97.216.167' : 'UPD-VIP2200-B2250-6-F5.telkomsel.co.id',\
 '10.97.216.168' : 'UPD-VIP2200-B2250-7-F5.telkomsel.co.id',\
 '10.114.231.240' : 'SUD-VIP2200-2250-1-F5.telkomsel.co.id',\
 '10.114.231.241' : 'SUD-VIP2200-2250-2-F5.telkomsel.co.id',\
 '10.114.231.242' : 'SUD-VIP2200-2250-3-F5.telkomsel.co.id',\
 '10.114.231.243' : 'SUD-VIP2200-2250-4-F5.telkomsel.co.id',\
 '10.114.231.244' : 'SUD-VIP2200-2250-5-F5.telkomsel.co.id',\
 '10.114.231.245' : 'SUD-VIP2200-2250-6-F5.telkomsel.co.id',\
 '10.114.231.246' : 'SUD-VIP2200-2250-7-F5.telkomsel.co.id',\
 '10.114.231.247' : 'SUD-VIP2200-2250-8-F5.telkomsel.co.id',\
 '10.114.231.248' : 'SUD-VIP2200-2250-9-F5.telkomsel.co.id',\
 '10.115.171.5' : 'TMK-10200V-1-F5.telkomsel.co.id',\
 '10.115.171.6' : 'TMK-10200V-2-F5.telkomsel.co.id',\
 '10.113.176.5' : 'JPR-10200V-1-F5.telkomsel.co.id',\
 '10.113.176.6' : 'JPR-10200V-2-F5.telkomsel.co.id',\
 '10.251.146.97' : 'viprion1brn.telkomsel.com',\
 '10.251.146.98' : 'viprion2brn.telkomsel.com',\
 '10.251.71.129' : 'viprion1tbs.telkomsel.com',\
 '10.251.71.130' : 'viprion2tbs.telkomsel.com'\
}


def write_header(file):
	#create new empty file and add header
	str_to_write = ''
	with open(file, 'w') as f:
		for i in range(len(headers)):
			str_to_write += headers[i] + ", "
		f.write(str_to_write.rstrip(", ") + "\n")

def print_header():
	print "Hostname, HA Status, Sync Status, Blades Status, Uptime, Disk Space, Chassis Temperature, CRC Error, CPU, Memory, Throughput, Connections, TCP Reset\n"

def send_alert(host, error):
	F5_hostname = hosts.get(host, host)
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((telegram_cli, 8001))

	s.send("msg PSO_DDI_F5 " + F5_hostname + ' ' + error + '\n')
	s.close()

def check_cpu(host,retval):
	#we use sysMultiHostCpuUsageRatio5m (.1.3.6.1.4.1.3375.2.1.7.5.2.1.35) for collecting CPU usage of each core, then calculate manually.
	retry = 0
	cpu_usage = 0
	while retry < retry_counter:
		sum=0
		p1 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.7.5.2.1.35 -Ov | awk -F ': ' '{print $2}'", shell=True, stdout=PIPE, stderr=PIPE)
		stdout1 = p1.communicate()[0].split()
		if stdout1:
			numCore = len(stdout1)
			for coreUsage in stdout1:
				sum += int(coreUsage.strip())
			cpu_usage = sum / numCore
			break
		else:
			retry += 1



	if cpu_usage >= 80 and isAlert:
		send_alert(host, "cpu is high")
	elif retry == retry_counter:
		retval.put('')
		if isAlert:
			send_alert(host, "cpu info cannot be fetched via snmp")
		return

	retval.put("%d%%" %(cpu_usage))


def human_output(number):
	number = int(number)
	if (number / (1000 * 1000 * 1000)) >= 1:
		result = "%.2fG" %(number / (1000 * 1000 * 1000 * 1.0))
	elif (number / (1000 * 1000)) >= 1:
		result = "%.2fM" %(number / (1000 * 1000 * 1.0))
	elif (number / 1000) >= 1:
		result = "%.2fK" %(number / (1000 * 1.0))
	else:
		result = number

	return str(result)


def check_sysGlobalStats(host, retval):
	retry = 0
	sysGlobalStats = dict()
	sysGlobalStats['memUsage'] = ''
	sysGlobalStats['thpBits'] = ''
	sysGlobalStats['curConns'] = ''
	memUsage = 0
	tmmMemUsage = 0
	tmmMemTotal = 0
	thpBits = 0
	curConns = 0
	stdout = ''
	while retry < retry_counter:
		p = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.1.2.1", shell=True, stdout=PIPE, stderr=PIPE)
		stdout = p.communicate()[0]
		if stdout:
			break
		else:
			retry += 1
	if retry < retry_counter:
		for line in stdout.rstrip('\n').split('\n'):
			if re.search('3375.2.1.1.2.1.144', line):
				try:
					tmmMemUsage = int(line.split('Counter64:')[1].strip())
				except:
					tmmMemUsage = int(line.split('Gauge32:')[1].strip())

			elif re.search('3375.2.1.1.2.1.143', line):
				try:
					tmmMemTotal = int(line.split('Counter64:')[1].strip())
				except:
					tmmMemTotal = int(line.split('Gauge32:')[1].strip())

			elif re.search('3375.2.1.1.2.1.90', line):
				try:
					clientBytesOut5m = int(line.split('Counter64:')[1].strip())
				except:
					clientBytesOut5m = int(line.split('Gauge32:')[1].strip())

			elif re.search('3375.2.1.1.2.1.95', line):
				try:
					serverBytesIn5m = int(line.split('Counter64:')[1].strip())
				except:
					serverBytesIn5m = int(line.split('Gauge32:')[1].strip())

			elif re.search('3375.2.1.1.2.1.8', line):
				try:
					curConns = int(line.split('Counter64:')[1].strip())
				except:
					curConns = int(line.split('Gauge32:')[1].strip())


		sysGlobalStats['memUsage'] = str(tmmMemUsage * 100 / tmmMemTotal) + '%'
		sysGlobalStats['thpBits'] = human_output((clientBytesOut5m + serverBytesIn5m) * 8)
		sysGlobalStats['curConns'] = human_output(curConns)

	elif isAlert:
		send_alert(host, "sysGlobalStats info cannot be fetched via snmp, memory/thpBits/CurConns affected")

	retval.put(sysGlobalStats)

	if sysGlobalStats['memUsage'] and int(sysGlobalStats['memUsage'].rstrip('%')) >= 80 and isAlert:
		send_alert(host, "memory is high")


def check_hdd(host, retval):
	retry = 0
	hd_status = ''
	hd_used = dict()
	hd_size = dict()
	hd_desc = dict()
	while retry < retry_counter:
		p = Popen(dd_walk + host + " .1.3.6.1.2.1.25.2.3.1", shell=True, stdout=PIPE, stderr=PIPE)
		stdout = p.communicate()[0]

		if stdout:
			for line in stdout.rstrip('\n').split('\n'):
				line = line.lower()
				if re.search('hrstorageused', line):
					key = line.split('=')[0].split('::')[1].split('.')[1].strip()
					value = line.split('integer:')[1].strip()
					hd_used[key] = value
				elif re.search('hrstoragesize', line):
					key = line.split('=')[0].split('::')[1].split('.')[1].strip()
					value = line.split('integer:')[1].strip()
					hd_size[key] = value
				elif re.search('hrstoragedescr', line):
					key = line.split('=')[0].split('::')[1].split('.')[1].strip()
					value = line.split('string:')[1].strip()
					hd_desc[key] = value

			try:
				hd_status = 'OK'
				for key in hd_desc:
					if "/" in hd_desc[key]:
						percent_used = int(hd_used[key]) * 100 / int(hd_size[key])
						if percent_used >= 80:
							hd_status = 'NOK'
							break
			except:
				hd_status = "check script"

			break

		else:
			retry += 1


	retval.put(hd_status)

	if hd_status == 'NOK' and isAlert:
		send_alert(host, "HDD status is NOK")
	elif retry == retry_counter and isAlert:
		send_alert(host, "HDD info cannot be fetched via snmp")


def check_blades(host, retval):
	retry = 0
	status = ''

	while retry < retry_counter:
		p1 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.10", shell=True, stdout=PIPE, stderr=PIPE)
		stdout1 = p1.communicate()[0]

		if stdout1:
			#this tot_Blades calculate how many serial number of blades detected on chassis
			tot_Blades = len(re.findall("3375.2.1.10.3.2.1.2.(.*) = STRING:", stdout1))
			avail = len(re.findall("3375.2.1.10.2.2.1.3.(.*)INTEGER: 1", stdout1))
			state = len(re.findall("3375.2.1.10.2.2.1.8.(.*)INTEGER: 3", stdout1))
			lic = len(re.findall("3375.2.1.10.2.2.1.7.(.*)INTEGER: 1", stdout1))
			ha = len(re.findall("3375.2.1.10.2.2.1.13.(.*)INTEGER: (3|4)", stdout1))
			reason = len(re.findall("3375.2.1.10.2.2.1.6.(.*)STRING: \"Run\"", stdout1))

			for line in stdout1.rstrip('\n').split('\n'):
				if re.search('3375.2.1.10.3.1.0', line):
					chassisSlotNumber = int(line.strip().split('INTEGER: ')[1])

			if chassisSlotNumber == 0: #nonModular BigIP10200
				status = "All Blades UP(1)"
			elif (avail == state == lic == ha == reason == tot_Blades):
				status = "All Blades UP({0})".format(tot_Blades)
			else:
				status = 'NOK'

			break
		else:
			retry += 1

	retval.put(status)
	if status == 'NOK' and isAlert:
		send_alert(host, "one or more of the blades experiencing problem")
	elif retry == retry_counter and isAlert:
		send_alert(host, "Blades info cannot be fetched via snmp")


def check_crc(host, retval):
	retry = 0
	ListCrc1 = []
	ListCrc2 = []
	status = 'OK'

	while retry < retry_counter:
		p1 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.2.4.4.3.1.8 -Ov | awk -F ': ' '{print $2}'", shell=True, stdout=PIPE, stderr=PIPE)
		stdout1 = p1.communicate()[0].split()
		if stdout1:
			for crc in stdout1:
				ListCrc1.append(int(crc))
			break
		else:
			retry += 1

	time.sleep(F5_interval)
	retry = 0
	while retry < retry_counter:
		p2 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.2.4.4.3.1.8 -Ov | awk -F ': ' '{print $2}'", shell=True, stdout=PIPE, stderr=PIPE)
		stdout2 = p2.communicate()[0].split()
		if stdout2:
			for crc in stdout2:
				ListCrc2.append(int(crc))
			break
		else:
			retry += 1

	i = len(ListCrc1)
	j = 0
	while len(ListCrc1) == len(ListCrc2) and j < i:
		if ListCrc1[j] == ListCrc2[j]:
			j += 1
		else:
			status = 'NOK'
			break

	if status == 'NOK' and isAlert:
		send_alert(host, "one of the interfaces experiencing crc error")
	elif retry == retry_counter:
		status = ''
		if isAlert:
			send_alert(host, "CRC info cannot be fetched via snmp")


	retval.put(status)


def check_chassisTemp(host, retval):
	retry = 0
	status = 'OK'
	platform_info = None
	temp_limit = None
	
	#chassis platform code: J100, blade platform code:A107
	vip4400_threshold = {\
		1 : 70, #"FanTray" \
		2 : 65 #"Annunciator" \
	}

	#chassis platform code: S100, blade platform code: A108
	vip4800_threshold = {\
		1 : 65, #"AnnunciatorInlet" \
		2 : 65, #"AnnunciatorOutlet" \
		3 : 65, #"Annunciator2Inlet" \
		4 : 70, #"Annunciator2Outlet" \
		5 : 70, #"FanTrayInlet" \
		6 : 70, #"FanTrayOutlet" \
		7 : 70, #"FanTray2Inlet" \
		8 : 70 #"FanTray2Outlet" \
	}


	#nonModular, blade platform code: D113
	bigip10200_threshold = {\
		1 : 70, #"MainBoardHSBE_TransistorTempe" \
		2 : 53, #"MainBoardHSBE_IC_Temperature" \
		3 : 39, #"MainBoardInletTransistorTemp" \
		4 : 55, #"MainBoardInlet_IC_Temperature" \
		5 : 61, #"MainBoardOutletTransistorTem" \
		6 : 61, #"MainBoardOutlet_IC_Temperature" \
		7 : 57, #"PECI_Bridge_LocalTemperature" \
		8 : 70, #"MezzanineBoard_HSBE_Transistor" \
		9 : 53, #"MezzanineBoard_HSBE_IC_Temperat" \
		10 : 63 #"MainBoardNearTridentB56843" \
	}
	
	while retry < retry_counter:
		p1 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.3.5.1.0 -Ov | awk -F ': ' '{print $2}'", shell=True, stdout=PIPE, stderr=PIPE)
		stdout1 = p1.communicate()[0].split()
		if stdout1:
			platform_info = stdout1[0].strip('"')
			break
		else:
			retry += 1


	#if the system uses viprion 2200-blade2250, the system will always report 'OK', as the box lack of chassis temperature sensor.
	if platform_info and platform_info != "A112":
		retry = 0
		while retry < retry_counter:
			p2 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.3.2.3.2.1.2 -Ov | awk -F ': ' '{print $2}' | sed 's/^0*//'", shell=True, stdout=PIPE, stderr=PIPE)
			stdout2 = p2.communicate()[0].split()
			if stdout2:
				if platform_info == "A107":
					temp_limit = vip4400_threshold
				elif platform_info == "A108":
					temp_limit = vip4800_threshold
				elif platform_info == "D113":
					temp_limit = bigip10200_threshold
				
				idx_max = len(temp_limit)
				
				for i in range(idx_max):
					idx_hi_limit = i + 1
					if int(stdout2[i].strip()) > temp_limit[idx_hi_limit]:
						status = "NOK"
						break
				break
			else:
				retry += 1


	if status != 'OK' and isAlert:
		send_alert(host, "chassis temperature is high")
	elif retry == retry_counter:
		status = ''
		if isAlert:
			send_alert(host, "ChassisTemperature info cannot be fetched via snmp")

	retval.put(status)


def tcpRst_count(host):
	#The cause for TCP/IP reset (RST) ==> SNMP OID .1.3.6.1.4.1.3375.2.2.11.1.3.1.2
	#The count for each TCP/IP reset (RST) cause ==> SNMP OID .1.3.6.1.4.1.3375.2.2.11.1.3.1.3
	RST_CNT = {
		'RST_TCP_3WS_RJCT' : '0',
		'RST_TCP_3WS_RJCT_ACK' : '0',
		'RST_TCP_3WS_RJCT_SEQ' : '0',
		'RST_TCP_3WS_RJCT_FLG' : '0',
		'RST_TCP_EARLY_FIN' : '0',
		'RST_TCP_CLOSED' : '0',
		'RST_TCP_KEEP_A_TMOUT' : '0',
		'RST_TCP_RETR_TMOUT' : '0',
		'RST_TCP_BAD_FLG' : '0',
		'RST_TCP_ZERO_TMOUT' : '0',
		'RST_TCP_RST_REMOTE' : '0',
		'RST_SWEEPER' : '0',
		'RST_NO_SRV_ROUTE' : '0',
		'RST_NO_CLT_ROUTE' : '0',
		'RST_ICMP_UNREACH' : '0',
		'RST_SYN_TMOUT' : '0',
		'RST_NO_POOL' : '0'
	}

	RST_VAR_STRING_MAP = {
		'RST_TCP_3WS_RJCT' : 'TCP 3WHS rejected',
		'RST_TCP_3WS_RJCT_ACK' : 'TCP 3WHS rejected (bad ACK)',
		'RST_TCP_3WS_RJCT_SEQ' : 'TCP 3WHS rejected (bad SEQ)',
		'RST_TCP_3WS_RJCT_FLG' : 'TCP 3WHS rejected (bad flags)',
		'RST_TCP_EARLY_FIN' : 'TCP early FIN',
		'RST_TCP_CLOSED' : 'TCP closed',
		'RST_TCP_KEEP_A_TMOUT' : 'TCP keep-alive timeout',
		'RST_TCP_RETR_TMOUT' : 'TCP retransmit timeout',
		'RST_TCP_BAD_FLG' : 'TCP bad flags',
		'RST_TCP_ZERO_TMOUT' : 'TCP zero window timeout',
		'RST_TCP_RST_REMOTE' : 'TCP RST from remote system',
		'RST_SWEEPER' : 'Flow expired (sweeper)',
		'RST_NO_SRV_ROUTE' : 'No route to host',
		'RST_NO_CLT_ROUTE' : 'No return route to client',
		'RST_ICMP_UNREACH' : 'ICMP unreachable received',
		'RST_SYN_TMOUT' : 'SYN timeout (server not responding)',
		'RST_NO_POOL' : 'No pool member available'
	}

	retry1 = 0
	while retry1 < retry_counter:
		p1 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.2.11.1.3.1.2 -Ov | awk -F ': ' '{print  $2}'", shell=True, stdout=PIPE, stderr=PIPE)
		stdout1 = p1.communicate()[0]
		if stdout1:
			LIST_RST_CAUSE_STRING = stdout1.rstrip('\n').split('\n')
			for i in range(0, len(LIST_RST_CAUSE_STRING)):
				LIST_RST_CAUSE_STRING[i] = LIST_RST_CAUSE_STRING[i].strip('"')
			break
		else:
			retry1 += 1

	retry2 = 0
	while retry2 < retry_counter:
		p2 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.2.11.1.3.1.3 -Ov | awk -F ': ' '{print $2}'", shell=True, stdout=PIPE, stderr=PIPE)
		stdout2 = p2.communicate()[0]
		if stdout2:
			LIST_RST_CNT = stdout2.rstrip('\n').split('\n')
			break
		else:
			retry2 += 1

	if retry1 < 3 and retry2 < 3:
		i = 0
		RST_STRING_CNT_MAP = dict()
		while len(LIST_RST_CAUSE_STRING) == len(LIST_RST_CNT) and i < len(LIST_RST_CNT):
			RST_STRING_CNT_MAP[LIST_RST_CAUSE_STRING[i]] = LIST_RST_CNT[i]
			i += 1

		for key in RST_STRING_CNT_MAP.keys():
			if key in RST_VAR_STRING_MAP.values():
				x = RST_VAR_STRING_MAP.keys()[RST_VAR_STRING_MAP.values().index(key)]
				RST_CNT[x] = RST_STRING_CNT_MAP[key]
	elif isAlert:
		send_alert(host, "TCP-Reset info cannot be fetched via snmp")
		#print ''

	return RST_CNT

def check_tcpRst(host, retval):
	tcpRst_count1 = tcpRst_count(host)
	time.sleep(F5_interval)
	tcpRst_count2 = tcpRst_count(host)

	str1 = ''
	for key in tcpRst_count1.keys():
		deltaRst = int(tcpRst_count2[key]) - int(tcpRst_count1[key])
		if deltaRst > 10000:
			str1 += key + " is counting " + str(deltaRst) + " in 10seconds || "

	str1 = str1.rstrip(" || ")
	retval.put(str1)


#haStatus is a state of the box whether ACTIVE or STANDBY
def get_haStatus(host, retval):
	retry = 0
	haStatus = ''
	while retry < retry_counter:
		p = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.14.3.2", shell=True, stdout=PIPE, stderr=PIPE)
		stdout = p.communicate()[0]
		if stdout:
			haStatus = stdout.split('STRING:')[1].strip().strip('"')
			break
		else:
			retry += 1

	retval.put(haStatus)

	if retry == retry_counter and isAlert:
		send_alert(host, "haStatus info cannot be fetched via snmp")


def get_syncStatus(host, retval):
	retry = 0
	syncStatus = ''
	while retry < retry_counter:
		p = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.14.1.2", shell=True, stdout=PIPE, stderr=PIPE)
		stdout = p.communicate()[0]
		if stdout:
			syncStatus = stdout.split('STRING:')[1].strip().strip('"')
			break
		else:
			retry += 1

	retval.put(syncStatus)
	if retry == retry_counter and isAlert:
		send_alert(host, "syncStatus info cannot be fetched via snmp")


def get_uptime(host, retval):
	retry = 0
	uptime = ''
	while retry < retry_counter:
		p = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.6.7 -Ov", shell=True, stdout=PIPE, stderr=PIPE)
		stdout = p.communicate()[0]
		if stdout:
			uptimeSec = stdout.split()[1].strip()
			uptime = str(datetime.timedelta(seconds=int(uptimeSec)))
			uptime = uptime.split(',')[0]
			break
		else:
			retry += 1

	retval.put(uptime)
	if retry == retry_counter and isAlert:
		send_alert(host, "uptime info cannot be fetched via snmp")


def print_to_screen(host, functions):
	str_to_print = hosts.get(host) + ','
	for func in functions:
		hostfunc = host + func.__name__
		dretval_get[hostfunc] = dretval[hostfunc].get()
		if func.__name__ == "check_sysGlobalStats":
			str1 = ''
			sysGlobalStats = dretval_get[hostfunc]
			for key in ('memUsage', 'thpBits', 'curConns'):
				str1 += sysGlobalStats[key] + ','
			str_to_print += str1
		else:
			str_to_print += dretval_get[hostfunc] + ','

	print str_to_print.rstrip(',')

def write_to_file(host, functions, file):
	hostname = hosts.get(host)
	str_to_write = hostname + ','
	for func in functions:
		hostfunc = host + func.__name__
		dretval_get[hostfunc] = dretval[hostfunc].get()
		if func.__name__ == "check_sysGlobalStats":
			str1 = ''
			sysGlobalStats = dretval_get[hostfunc]
			for key in ('memUsage', 'thpBits', 'curConns'):
				str1 += sysGlobalStats[key] + ','
			str_to_write += str1
		else:
			str_to_write += dretval_get[hostfunc] + ','

	dictEntry[hostname] = str_to_write


class funcThread (threading.Thread):
	def __init__(self, host, func, retval):
		threading.Thread.__init__(self)
		self.host = host
		self.func = func
		self.retval = retval

	def run(self):
		self.func(self.host, self.retval)

class printThread(threading.Thread):
	def __init__(self, host, funcs):
		threading.Thread.__init__(self)
		self.host = host
		self.funcs = funcs

	def run(self):
		print_to_screen(self.host, self.funcs)

class writeThread(threading.Thread):
	def __init__(self, host, funcs, file):
		threading.Thread.__init__(self)
		self.host = host
		self.funcs = funcs
		self.file = file

	def run(self):
		write_to_file(self.host, self.funcs, self.file)


funcs = (get_haStatus, get_syncStatus, check_blades, get_uptime, check_hdd, check_chassisTemp, check_crc, check_cpu, check_sysGlobalStats, check_tcpRst)

lthreads = []
lprint_threads = []
lwrite_threads = []

dthreads = {}
dprint_threads = {}
dwrite_threads = {}

dretval = {}
dretval_get = {}


def main():
	if len(sys.argv) > 1 and sys.argv[1] == 'print':
		print_header()
		for host in hosts:
			for func in funcs:
				hostfunc = host + func.__name__
				dretval[hostfunc] = Queue()
				dthreads[hostfunc] = funcThread(host, func, dretval[hostfunc])
				dthreads[hostfunc].start()
				lthreads.append(dthreads[hostfunc])

		for t in lthreads:
			t.join()

		for host in hosts:
			dprint_threads[host] = printThread(host, funcs)
			dprint_threads[host].start()
			lprint_threads.append(dprint_threads[host])

		for t in lprint_threads:
			t.join()

	elif len(sys.argv) > 1 and sys.argv[1] == 'csv':
		write_header(output_csv)
		for host in hosts:
			for func in funcs:
				hostfunc = host + func.__name__
				dretval[hostfunc] = Queue()
				dthreads[hostfunc] = funcThread(host, func, dretval[hostfunc])
				dthreads[hostfunc].start()
				lthreads.append(dthreads[hostfunc])

		for t in lthreads:
			t.join()

		for host in hosts:
			dwrite_threads[host] = writeThread(host, funcs, output_csv)
			dwrite_threads[host].start()
			lwrite_threads.append(dwrite_threads[host])

		for t in lwrite_threads:
			t.join()

		with open(output_csv,'a') as f:
			for key in sorted(dictEntry):
				f.write(dictEntry[key].rstrip(',') + '\n')


	elif len(sys.argv) > 1 and sys.argv[1] == 'xlsx':
		for host in hosts:
			for func in funcs:
				hostfunc = host + func.__name__
				dretval[hostfunc] = Queue()
				dthreads[hostfunc] = funcThread(host, func, dretval[hostfunc])
				dthreads[hostfunc].start()
				lthreads.append(dthreads[hostfunc])

		for t in lthreads:
			t.join()

		for host in hosts:
			dwrite_threads[host] = writeThread(host, funcs, output_csv)
			dwrite_threads[host].start()
			lwrite_threads.append(dwrite_threads[host])

		for t in lwrite_threads:
			t.join()
			
		#print "finish generate dictEntry => " + str(datetime.datetime.now()) + '\n'
		
		wb = Workbook()
		ws1 = wb.active
		ws1.title = "F5 Health Check"
		
		fill_header = PatternFill(fill_type='solid', start_color='FF66BC29', end_color='FF66BC29')
		#fill_odd = PatternFill(fill_type='solid', start_color='FFFFEBAB', end_color='FFFFEBAB')
		fill_active = PatternFill(fill_type='solid', start_color='FFFFEBAB', end_color='FFFFEBAB')

		fill_blue = PatternFill(fill_type='solid', start_color='FF0070C0', end_color='FF0070C0')
		fill_light_blue = PatternFill(fill_type='solid', start_color='FF00B0F0', end_color='FF00B0F0')
		fill_yellow = PatternFill(fill_type='solid', start_color='FFFFFF00', end_color='FFFFFF00')
		fill_orange = PatternFill(fill_type='solid', start_color='FFFFC000', end_color='FFFFC000')
		fill_red = PatternFill(fill_type='solid', start_color='FFFF0000', end_color='FFFF0000')
		fill_dark_red = PatternFill(fill_type='solid', start_color='FFC00000', end_color='FFC00000')
		fill_white = PatternFill(fill_type='solid', start_color='FFFFFFFF', end_color='FFFFFFFF')
		
		ws1['A1'] = "Hostname"
		ws1['B1'] = "HA Status"
		ws1['C1'] = "Sync Status"
		ws1['D1'] = "Blades Status"
		ws1['E1'] = "Uptime"
		ws1['F1'] = "Disk Space"
		ws1['G1'] = "Chassis Temperature"
		ws1['H1'] = "CRC Error"
		ws1['I1'] = "CPU"
		ws1['J1'] = "Memory"
		ws1['K1'] = "Throughput"
		ws1['L1'] = "Connections"
		ws1['M1'] = "TCP Reset"
		
		for col in range(1, len(headers) + 1):
			ws1.cell(column=col, row=1).fill = fill_header
			
		thin_border = Border(left=Side(border_style='thin', color='FF000000'), \
			right=Side(border_style='thin', color='FF000000'), \
			top=Side(border_style='thin', color='FF000000'), \
			bottom=Side(border_style='thin', color='FF000000'))
			
		ws1_alignment=Alignment(vertical='center',
			wrap_text=False,
			shrink_to_fit=False)

		for row in range(1, len(hosts) + 2):
			for col in range (1, len(headers) + 1):
				ws1.cell(column=col, row=row).border = thin_border
				ws1.cell(column=col, row=row).alignment = ws1_alignment			
		
		#print "finish populate empty table => " + str(datetime.datetime.now()) + '\n'
		
		row = 2
		#print sorted(dictEntry)
		#print "\n\n"
		for key in sorted(dictEntry):
			host_entries_list = dictEntry[key].split(',')
			#print host_entries_list
			#print "\n\n"
			for col in range(1, len(headers) + 1):
				idx = col - 1
				cell_entry = str(host_entries_list[idx])
				if cell_entry == '':
					ws1.cell(column=col, row=row).fill = fill_blue
				else:
					ws1.cell(row=row, column=col, value="%s" % cell_entry)
					if headers[idx].lower() == "ha status" and cell_entry.lower() == "active":
						ws1.cell(column=col, row=row).fill = fill_active
					elif headers[idx].lower() == "sync status" and cell_entry.lower() == "changes pending":
						ws1.cell(column=col, row=row).fill = fill_yellow
					elif headers[idx].lower() == "blades status" and cell_entry.lower() == "nok":
						ws1.cell(column=col, row=row).fill = fill_red
					elif headers[idx].lower() == "disk space" and cell_entry.lower() == "nok":
						ws1.cell(column=col, row=row).fill = fill_red
					elif headers[idx].lower() == "chassis temperature" and cell_entry.lower() == "nok":
						ws1.cell(column=col, row=row).fill = fill_red
					elif headers[idx].lower() == "crc error" and cell_entry.lower() == "nok":
						ws1.cell(column=col, row=row).fill = fill_red
					elif (headers[idx].lower() == "cpu" or headers[idx].lower() == "memory") and cell_entry != '':
						#print str(headers[idx]) + " => " + str(cell_entry)
						fill_cell = fill_white					
						usage = int(cell_entry.rstrip('%'))
						if usage > 40 and usage <=60:
							fill_cell = fill_yellow
						elif usage > 60 and usage <=70:
							fill_cell = fill_orange
						elif usage > 70 and usage <=80:
							fill_cell = fill_red
						elif usage > 80:
							fill_cell = fill_dark_red
						
						ws1.cell(column=col, row=row).fill = fill_cell
				
				
			row += 1
		
		wb.save(output_xlsx)
		
		#print "finish input entries => " + str(datetime.datetime.now()) + '\n'

	else:
		print "usage: ./script.py args\n\nplease input argument:\n  print => output will be printed on screen in csv format\n  csv => will produce .csv file\n  xlsx => will produce .xlsx file\n"

if __name__ == '__main__':
	main()
