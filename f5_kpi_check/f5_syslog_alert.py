#!/usr/bin/python
#this script is run as an action of splunk alert
#mailto: wahyu.herdyanto@dimensiondata.com
import sys
import gzip
import re
import socket
import datetime

timestamp = "{:%Y-%b-%d %H:%M:%S}".format(datetime.datetime.now())
#ChangeThis
telegram_cli = "$TELEGRAM_API_IPADDR"
err_logfile = "/apps/splunk/bin/scripts/f5_syslog_alert_err.log"
res_csv_gz = sys.argv[8]
#res_csv_gz = "/tmp/test.csv.gz"


fgz = gzip.open(res_csv_gz, 'rb')
res_csv = fgz.readlines()
fgz.close()	

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((telegram_cli, 8001))

f = open(err_logfile, 'a')

del res_csv[0]
try:
	for line in res_csv:
		raw_log = line.split(',')[3]
		if len(raw_log.split(' ')) >= 4:
			hostname = raw_log.split(' ')[7]
			if '/' in hostname:
				hostname = hostname.split('/')[1]
			
			log_entry = re.split("\[[0-9]+\]\: .+\:[0-9]+\:", raw_log)[1]
			str_to_send = hostname + log_entry[:300] + '\n'
			s.send("msg PSO_DDI_F5 " + str_to_send)

except Exception as e:
	str_to_send = "SPLUNK error on executing the script\n"
	s.send("msg PSO_DDI_F5 " + str_to_send)
	f.write(timestamp + ' ' e.message + '\n')

f.close()
s.close()