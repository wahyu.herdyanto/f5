#!/usr/bin/python
#mailto: wahyu.herdyanto@dimensiondata.com
#last update: 28-Apr-2017

import time
import re
import threading
import sys
import socket
from Queue import Queue
from subprocess import Popen
from subprocess import PIPE


########################################################################
hosts = ("10.77.113.46", "10.77.113.47", "10.77.113.48", "10.77.113.49", "10.60.100.43", "10.60.100.46", "10.60.100.49",\
 "10.114.231.240", "10.114.231.241", "10.114.231.242", "10.114.231.243", "10.97.216.164",\
 "10.35.213.96", "10.35.213.97", "10.35.213.98", "10.35.185.6",\
 "10.251.15.77", "10.251.15.93", "10.251.15.107", "10.251.15.108",\
 "10.251.165.77", "10.251.165.93", "10.251.165.107", "10.251.165.108")

#F5_check_output = "/tmp/F5_check_output.txt"
#F5_check_crc_output = "/tmp/F5_check_crc_output.txt"
dd_walk = "snmpwalk -v2c -c F5_PDN "
telegram_cli = "10.251.16.138"

#this interval is for traffic bps / connection collection, in seconds
F5_crc_interval = 10


def send_alert(host, error):
	p = Popen("snmpget -v2c -c F5_PDN " + host + " .1.3.6.1.4.1.3375.2.1.6.2.0", shell=True, stdout=PIPE, stderr=PIPE)
	stdout = p.communicate()[0]
	F5_hostname = stdout.split('STRING: ')[1].rstrip().strip('\"')
	F5_sitename = F5_hostname.split('-')[0]
	if F5_sitename == 'JPR' or F5_sitename == 'TMK':
		F5_nodeID = F5_hostname.split('-')[2]
	else:
		F5_nodeID = F5_hostname.split('-')[3]
	
	finalName = "F5-PDN-" + F5_sitename + '-' + F5_nodeID
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((telegram_cli, 8001))

	s.send("msg PSO_DDI_F5 " + finalName + ' ' + error + '\n')
	s.close()
	
def check_cpu(host, retval):
	#we use sysMultiHostCpuUsageRatio5m (.1.3.6.1.4.1.3375.2.1.7.5.2.1.35) for collecting CPU usage of each core, then calculate manually.
	sum=0	
	p1 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.7.5.2.1.35 -Ov | awk -F ': ' '{print $2}'", shell=True, stdout=PIPE, stderr=PIPE)
	stdout1 = p1.communicate()[0].split()
	numCore = len(stdout1)
	for coreUsage in stdout1:
		sum += int(coreUsage.strip())
	cpu_usage = sum / numCore
	retval.put("%d%%" %(cpu_usage))
	if cpu_usage >= 80:
		send_alert(host, "cpu is high")
	

def check_memory(host, retval):
	p1 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.1.2.1.144 -Ov | awk -F ': ' '{print $2}'", shell=True, stdout=PIPE, stderr=PIPE)
	stdout1 = p1.communicate()[0].split()
	
	p2 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.1.2.1.143 -Ov | awk -F ': ' '{print $2}'", shell=True, stdout=PIPE, stderr=PIPE)
	stdout2 = p2.communicate()[0].split()
	
	tmmMemUsage = int(stdout1[0].strip())
	tmmMemTotal = int(stdout2[0].strip())
	mem_usage = tmmMemUsage * 100 / tmmMemTotal
	retval.put("%d%%" %(mem_usage))
	
	if mem_usage >= 80:
		send_alert(host, "memory is high")
	
def check_blades(host, retval):
	p1 = Popen("snmpget -v2c -c F5_PDN " + host + " .1.3.6.1.4.1.3375.2.1.10.3.1.0 -Ov | awk '{print $2}'", shell=True, stdout=PIPE, stderr=PIPE)
	stdout1 = p1.communicate()[0].split()
	chassisSlotNumber = int(stdout1[0])
	if chassisSlotNumber == 0:
		#it means the box is not modular, so we can skip this blade's checking and echo "OK"
		retval.put("OK")
	else:
		#this tot_Blades calculate how many serial number of blades detected on chassis
		p2 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.10.3.2.1.2 -Ov | awk -F ': ' '{print $2}' | sed '/^$/d'", shell=True, stdout=PIPE, stderr=PIPE)
		stdout2 = p2.communicate()[0].split()
		tot_Blades = len(stdout2)
		
		p3 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.10.2.2.1.3 -Ov | awk -F ': ' '{print $2}' | grep 1", shell=True, stdout=PIPE, stderr=PIPE)
		stdout3 = p3.communicate()[0].split() 
		
		p4 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.10.2.2.1.8 -Ov | awk -F ': ' '{print $2}' | grep 3", shell=True, stdout=PIPE, stderr=PIPE)
		stdout4 = p4.communicate()[0].split()
		
		p5 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.10.2.2.1.7 -Ov | awk -F ': ' '{print $2}' | grep 1", shell=True, stdout=PIPE, stderr=PIPE)
		stdout5 = p5.communicate()[0].split()
		
		p6 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.10.2.2.1.13 -Ov | awk -F ': ' '{print $2}' | egrep '3|4'", shell=True, stdout=PIPE, stderr=PIPE)
		stdout6 = p6.communicate()[0].split()
		
		p7 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.10.2.2.1.6 -Ov | awk -F ': ' '{print $2}' | grep 'Run'", shell=True, stdout=PIPE, stderr=PIPE)
		stdout7 = p7.communicate()[0].split()
		
		avail = len(stdout3)
		state = len(stdout4)
		lic = len(stdout5)
		ha = len(stdout6)
		reason = len(stdout7)
		
		if avail == state == lic == ha == reason == tot_Blades:
			status = 'OK'
			
		else:
			status = 'NOK'
		
		retval.put(status)
		
		if status != 'OK':
			send_alert(host, "one or more of the blades experiencing problem")
		

def check_crc(host, retval):
	ListCrc1 = []
	ListCrc2 = []
	status = 'OK'
	
	p1 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.2.4.4.3.1.8 -Ov | awk -F ': ' '{print $2}'", shell=True, stdout=PIPE, stderr=PIPE)
	stdout1 = p1.communicate()[0].split()
	
	for crc in stdout1:
		ListCrc1.append(int(crc))
	
	time.sleep(F5_crc_interval)
	
	p2 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.2.4.4.3.1.8 -Ov | awk -F ': ' '{print $2}'", shell=True, stdout=PIPE, stderr=PIPE)
	stdout2 = p2.communicate()[0].split()
	for crc in stdout2:
		ListCrc2.append(int(crc))
	
	i = len(ListCrc1)
	j = 0
	while len(ListCrc1) == len(ListCrc2) and j < i:
		if ListCrc1[j] == ListCrc2[j]:
			j += 1
		else:
			status = 'NOK'
			break

	retval.put(status)
	if status != 'OK':
		send_alert(host, "one of the interfaces experiencing crc error")
	

def check_chassisTemp(host, retval):
	temp_limit = 40
	status = "OK"
	p1 = Popen("snmpget -v2c -c F5_PDN " + host + " .1.3.6.1.4.1.3375.2.1.3.5.1.0 -Ov | awk -F ': ' '{print $2}'", shell=True, stdout=PIPE, stderr=PIPE)
	stdout1_list = p1.communicate()[0].split()
	platform_info = stdout1_list[0].strip('"')
	#if the system uses viprion 2200-blade2250, the system will always report 'OK', as the box lack of chassis temperature sensor.
	if platform_info != "A112":
		p2 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.3.2.3.2.1.2 -Ov | awk -F ': ' '{print $2}' | sed 's/^0*//'", shell=True, stdout=PIPE, stderr=PIPE)
		stdout2_list = p2.communicate()[0].split()
		for temp in stdout2_list:
			if int(temp.strip()) > temp_limit:
				status = "NOK"
				break
	retval.put(status)
	if status != 'OK':
		send_alert(host, "chassis temperature is high")
		

def check_thpBits(host, retval):
	p1 = Popen("snmpget -v2c -c F5_PDN " + host + " .1.3.6.1.4.1.3375.2.1.3.5.1.0 -Ov | awk -F ': ' '{print $2}'", shell=True, stdout=PIPE, stderr=PIPE)
	stdout1_list = p1.communicate()[0].split()
	platform_info = stdout1_list[0].strip('"')
	
	if platform_info == 'D113':
		totBlades = 1
	else:
		p0 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.10.3.2.1.2 -Ov | awk -F ': ' '{print $2}' | sed '/^$/d'", shell=True, stdout=PIPE, stderr=PIPE)
		stdout0 = p0.communicate()[0].rstrip('\n').split()
		totBlades = len(stdout0)
	
	#since blade 2250 has different capacity sizing in term of throughput, we need to differentiate it.
	#A112 := PB2250
	#A108 := PB4300
	#D113 := 10200V
	if platform_info == 'A112':
		thp_box_capacity = totBlades * 10000000000
	else:
		thp_box_capacity = totBlades * 6750000000
	
	p2 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.1.2.1", shell=True, stdout=PIPE, stderr=PIPE)
	stdout2 = p2.communicate()[0].rstrip('\n').split('\n')
	for line in stdout2:
		if re.search('2.1.1.2.1.3.0', line):
			clientBytesIn = int(line.strip().split('Counter64: ')[1])
		elif re.search('2.1.1.2.1.5.0', line):
			clientBytesOut = int(line.strip().split('Counter64: ')[1])
		elif re.search('2.1.1.2.1.10.0', line):
			serverBytesIn = int(line.strip().split('Counter64: ')[1])
		elif re.search('2.1.1.2.1.12.0', line):
			serverBytesOut = int(line.strip().split('Counter64: ')[1])
	
	####################################################################################
	# subs --A--> F5 --B--> Server
	# subs <--D-- F5 <--C-- Server
	
	#A
	clientBitsIn = clientBytesIn * 8
	
	#D
	clientBitsOut = clientBytesOut * 8
	
	#B
	serverBitsIn = serverBytesIn * 8
	
	#C
	serverBitsOut = serverBytesOut * 8
	
	retval.put((str(clientBitsIn), str(clientBitsOut), str(serverBitsIn), str(serverBitsOut), str(thp_box_capacity)))

def check_newConn(host, retval):
	p = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.1.1.2.1", shell=True, stdout=PIPE, stderr=PIPE)
	stdout = p.communicate()[0].rstrip('\n').split('\n')
	for line in stdout:
		if re.search('2.1.1.2.1.14.0', line):
			sysStatServerTotConns = int(line.strip().split('Counter64: ')[1])
		elif re.search('2.1.1.2.1.7.0', line):
			sysStatClientTotConns = int(line.strip().split('Counter64: ')[1])
	
	retval.put((str(sysStatServerTotConns), str(sysStatServerTotConns)))

def check_tcpRst(host, retval):
	#The cause for TCP/IP reset (RST) ==> SNMP OID .1.3.6.1.4.1.3375.2.2.11.1.3.1.2
	#The count for each TCP/IP reset (RST) cause ==> SNMP OID .1.3.6.1.4.1.3375.2.2.11.1.3.1.3

	RST_CNT = {
		'RST_TCP_3WS_RJCT' : '0',
		'RST_TCP_3WS_RJCT_ACK' : '0',
		'RST_TCP_3WS_RJCT_SEQ' : '0',
		'RST_TCP_3WS_RJCT_FLG' : '0',
		'RST_TCP_EARLY_FIN' : '0',
		'RST_TCP_CLOSED' : '0',
		'RST_TCP_KEEP_A_TMOUT' : '0',
		'RST_TCP_RETR_TMOUT' : '0',
		'RST_TCP_BAD_FLG' : '0',
		'RST_TCP_ZERO_TMOUT' : '0',
		'RST_TCP_RST_REMOTE' : '0',
		'RST_SWEEPER' : '0',
		'RST_NO_SRV_ROUTE' : '0',
		'RST_NO_CLT_ROUTE' : '0',
		'RST_ICMP_UNREACH' : '0',
		'RST_SYN_TMOUT' : '0',
		'RST_NO_POOL' : '0'
	}

	p1 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.2.11.1.3.1.2 -Ov | awk -F ': ' '{print  $2}'", shell=True, stdout=PIPE, stderr=PIPE)
	LIST_RST_CAUSE_STRING = p1.communicate()[0].rstrip('\n').split('\n')
	for i in range(0, len(LIST_RST_CAUSE_STRING)):
		LIST_RST_CAUSE_STRING[i] = LIST_RST_CAUSE_STRING[i].strip('"')

	p2 = Popen(dd_walk + host + " .1.3.6.1.4.1.3375.2.2.11.1.3.1.3 -Ov | awk -F ': ' '{print $2}'", shell=True, stdout=PIPE, stderr=PIPE)
	LIST_RST_CNT = p2.communicate()[0].rstrip('\n').split('\n')

	RST_VAR_STRING_MAP = {
		'RST_TCP_3WS_RJCT' : 'TCP 3WHS rejected',
		'RST_TCP_3WS_RJCT_ACK' : 'TCP 3WHS rejected (bad ACK)',
		'RST_TCP_3WS_RJCT_SEQ' : 'TCP 3WHS rejected (bad SEQ)',
		'RST_TCP_3WS_RJCT_FLG' : 'TCP 3WHS rejected (bad flags)',
		'RST_TCP_EARLY_FIN' : 'TCP early FIN',
		'RST_TCP_CLOSED' : 'TCP closed',
		'RST_TCP_KEEP_A_TMOUT' : 'TCP keep-alive timeout',
		'RST_TCP_RETR_TMOUT' : 'TCP retransmit timeout',
		'RST_TCP_BAD_FLG' : 'TCP bad flags',
		'RST_TCP_ZERO_TMOUT' : 'TCP zero window timeout',
		'RST_TCP_RST_REMOTE' : 'TCP RST from remote system',
		'RST_SWEEPER' : 'Flow expired (sweeper)',
		'RST_NO_SRV_ROUTE' : 'No route to host',
		'RST_NO_CLT_ROUTE' : 'No return route to client',
		'RST_ICMP_UNREACH' : 'ICMP unreachable received',
		'RST_SYN_TMOUT' : 'SYN timeout (server not responding)',
		'RST_NO_POOL' : 'No pool member available'
	}

	i = 0
	RST_STRING_CNT_MAP = {}
	while len(LIST_RST_CAUSE_STRING) == len(LIST_RST_CNT) and i < len(LIST_RST_CNT):
		RST_STRING_CNT_MAP[LIST_RST_CAUSE_STRING[i]] = LIST_RST_CNT[i]
		i += 1

	for key in RST_STRING_CNT_MAP.keys():
		if key in RST_VAR_STRING_MAP.values():
			x = RST_VAR_STRING_MAP.keys()[RST_VAR_STRING_MAP.values().index(key)]
			RST_CNT[x] = RST_STRING_CNT_MAP[key]
	 
	# value
	str_cnt = ''
	for key in RST_CNT.keys():
		str_cnt += (RST_CNT[key] + ';NULL;')

	retval.put(str_cnt.rstrip(';'))

#failoverStatus is a state of the box whether ACTIVE or STANDBY
def get_failoverStatus(host, retval):
	p = Popen("snmpget -v2c -c F5_PDN " + host + " .1.3.6.1.4.1.3375.2.1.14.3.2.0", shell=True, stdout=PIPE, stderr=PIPE)
	stdout = p.communicate()[0]
	retval.put(stdout.split('STRING: ')[1].rstrip().strip('\"'))

def get_finalName(host, retval):
	p = Popen("snmpget -v2c -c F5_PDN " + host + " .1.3.6.1.4.1.3375.2.1.6.2.0", shell=True, stdout=PIPE, stderr=PIPE)
	stdout = p.communicate()[0]
	F5_hostname = stdout.split('STRING: ')[1].rstrip().strip('\"')
	F5_sitename = F5_hostname.split('-')[0]
	if F5_sitename == 'JPR' or F5_sitename == 'TMK':
		F5_nodeID = F5_hostname.split('-')[2]
	else:
		F5_nodeID = F5_hostname.split('-')[3]
	
	finalName = "F5-PDN-" + F5_sitename + '-' + F5_nodeID
	retval.put(finalName)
	
def print_to_screen(host, functions):
	str_to_print = ''
	for func in functions:
		hostfunc = host + func.__name__
		dretval_get[hostfunc] = dretval[hostfunc].get()
		if func.__name__ == 'check_thpBits':
			str_to_print += dretval_get[hostfunc][0] + ';' + dretval_get[hostfunc][4] + ';' + dretval_get[hostfunc][1] + ';' + dretval_get[hostfunc][4] + ';' + dretval_get[hostfunc][2] + ';' + dretval_get[hostfunc][4] + ';' + dretval_get[hostfunc][3] + ';' + dretval_get[hostfunc][4] + ';'
		elif func.__name__ == 'check_newConn':
			str_to_print += dretval_get[hostfunc][0] + ';NULL;' + dretval_get[hostfunc][1] + ';NULL;'
		elif func.__name__ == 'get_finalName' or func.__name__ == 'check_tcpRst':
			str_to_print += dretval_get[hostfunc] + ';'
		else:
			str_to_print += dretval_get[hostfunc] + ';NULL;'
	print str_to_print + dretval_get[host + get_finalName.__name__] + '\n'
	
def crc_print_to_screen(host, functions):
	str_to_print = ''
	for func in functions:
		hostfunc = host + func.__name__
		crc_dretval_get[hostfunc] = crc_dretval[hostfunc].get()
		
		if func.__name__ == 'get_finalName':
			finalName = crc_dretval_get[hostfunc]
		else:
			str_to_print += crc_dretval_get[hostfunc] + ';NULL;'

	print finalName + ';' + str_to_print + finalName + '\n'

class funcThread (threading.Thread):
	def __init__(self, host, func, retval):
		threading.Thread.__init__(self)
		self.host = host
		self.func = func
		self.retval = retval

	def run(self):
		self.func(self.host, self.retval)
	
class printThread(threading.Thread):
	def __init__(self, host, funcs):
		threading.Thread.__init__(self)
		self.host = host
		self.funcs = funcs

	def run(self):
		print_to_screen(self.host, self.funcs)

class crc_printThread(threading.Thread):
	def __init__(self, host, funcs):
		threading.Thread.__init__(self)
		self.host = host
		self.funcs = funcs

	def run(self):
		crc_print_to_screen(self.host, self.funcs)


funcs = (get_finalName, get_failoverStatus, check_cpu, check_memory, check_blades, check_chassisTemp, check_thpBits, check_newConn, check_tcpRst)
crc_funcs = (get_finalName, check_crc)

lthreads = []
lprint_threads = []

dthreads = {}
dprint_threads = {}
dretval = {}
dretval_get = {}

crc_lthreads = []
crc_lprint_threads = []

crc_dthreads = {}
crc_dprint_threads = {}
crc_dretval = {}
crc_dretval_get = {}

def main():	
	if len(sys.argv) > 1 and sys.argv[1] == '1':
		for host in hosts:
			for func in funcs:
				hostfunc = host + func.__name__
				dretval[hostfunc] = Queue()
				dthreads[hostfunc] = funcThread(host, func, dretval[hostfunc])
				dthreads[hostfunc].start()
				lthreads.append(dthreads[hostfunc])

		for t in lthreads:
			t.join()

		for host in hosts:
			dprint_threads[host] = printThread(host, funcs)
			dprint_threads[host].start()
			lprint_threads.append(dprint_threads[host])

		for t in lprint_threads:
			t.join()
			
	elif len(sys.argv) > 1 and sys.argv[1] == '2':
		for host in hosts:
			for func in crc_funcs:
				hostfunc = host + func.__name__
				crc_dretval[hostfunc] = Queue()
				crc_dthreads[hostfunc] = funcThread(host, func, crc_dretval[hostfunc])
				crc_dthreads[hostfunc].start()
				crc_lthreads.append(crc_dthreads[hostfunc])

		for t in crc_lthreads:
			t.join()
		
		for host in hosts:
			crc_dprint_threads[host] = crc_printThread(host, crc_funcs)
			crc_dprint_threads[host].start()
			crc_lprint_threads.append(crc_dprint_threads[host])
		
		for t in crc_lprint_threads:
			t.join()
	else:
		print "please input argument (number only):\n 1 => all parameters checking, except crc\n 2 => crc parameter checking\n"
if __name__ == '__main__':
	main()
