#2015-09-21, created by wahyu.herdyanto@dimensiondata.com

#snmp command to get QPS statistic
#curSuccessQuery=`snmpwalk -v2c -c dnsgiro 10.97.216.18 .1.3.6.1.4.1.13315.3.1.2.2.2.1.1`
#curReferralQuery =`snmpwalk -v2c -c dnsgiro 10.97.216.18 .1.3.6.1.4.1.13315.3.1.2.2.2.1.2`
#curNxrrsetQuery =`snmpwalk -v2c -c dnsgiro 10.97.216.18 .1.3.6.1.4.1.13315.3.1.2.2.2.1.3`
#curNxdomainQuery =`snmpwalk -v2c -c dnsgiro 10.97.216.18 .1.3.6.1.4.1.13315.3.1.2.2.2.1.4`
#curRecursionQuery =`snmpwalk -v2c -c dnsgiro 10.97.216.18 .1.3.6.1.4.1.13315.3.1.2.2.2.1.5`
#curFailureQuery =`snmpwalk -v2c -c dnsgiro 10.97.216.18 .1.3.6.1.4.1.13315.3.1.2.2.2.1.6`
#curMinute=$(date '+%M')

#VS need to be created
#VS_UPD_DNS_53_10.168.0.0
#VS_UPD_DNS_53_10.172.0.0
#VS_UPD_DNS_53_10.176.0.0
#VS_UPD_DNS_53_10.180.0.0
#VS_UPD_DNS_53_10.168.0.0_TCP
#VS_UPD_DNS_53_10.172.0.0_TCP
#VS_UPD_DNS_53_10.176.0.0_TCP
#VS_UPD_DNS_53_10.180.0.0_TCP

#File need to be created
#/shared/script/bluecatCheckQPS/prevQ.txt
#/shared/script/bluecatCheckQPS/qps.txt

highThresholdQ=20000
lowThresholdQ=15000
sumQ=0
timeQ=0
arrPrevQ=()
arrCurrQ=()

for i in 0 1 2 3 4 5 6;
do
	let j="$i + 1"
	arrPrevQ[$i]=`sed -n $j'p' /shared/script/bluecatCheckQPS/prevQ.txt`
done
#echo ${arrLastQ[@]}

for i in 0 1 2 3 4 5 6;
do
	if [ $i == 6 ];then
		arrCurrQ[$i]=$(date '+%M')
	else
		let j="$i + 1"
		arrCurrQ[$i]=`snmpwalk -v2c -c dnsgiro 10.97.216.18 .1.3.6.1.4.1.13315.3.1.2.2.2.1.$j | awk '{ print $4 }'`
	fi
done

echo "arrCurrQ" ${arrCurrQ[@]}
echo "arrPrevQ" ${arrPrevQ[@]}


for i in 0 1 2 3 4 5 6;
do
	if [ $i == 6 ];then
		if [ ${arrCurrQ[$i]} -lt ${arrPrevQ[$i]} ];then
			timeQ=`expr '(' ${arrCurrQ[$i]} + 60 - ${arrPrevQ[$i]} ')' '*' 60`
		else
			timeQ=`expr '(' ${arrCurrQ[$i]} - ${arrPrevQ[$i]} ')' '*' 60`
		fi
	else
		((sumQ+=`expr ${arrCurrQ[$i]} - ${arrPrevQ[$i]}`))
	fi
done

qps=`expr $sumQ '/' $timeQ`
if [ $qps ];then
	echo `date` " " $qps >> /shared/script/bluecatCheckQPS/qps.txt
else
	echo `date` " zero QPS" >> /shared/script/bluecatCheckQPS/qps.txt
fi

sleep 1
$(> /shared/script/bluecatCheckQPS/prevQ.txt)

for i in 0 1 2 3 4 5 6;
do
	echo ${arrCurrQ[$i]} >> /shared/script/bluecatCheckQPS/prevQ.txt
done

arrVSL=(VS_UPD_DNS_53_10.168.0.0 VS_UPD_DNS_53_10.172.0.0 VS_UPD_DNS_53_10.176.0.0 VS_UPD_DNS_53_10.180.0.0)
count=0
while [ $count -lt 3 ]
do
	cacheInfo=`tmsh list ltm virtual ${arrVSL[$count]} | grep PROFILE | awk '{ print $1 }'`
	if [ $qps -gt $highThresholdQ ] && [ $cacheInfo == "UPD_DNS_PROFILE" ];then
		`tmsh modify ltm virtual ${arrVSL[$count]} profiles delete { UPD_DNS_PROFILE } profiles add  { UPD_DNS_CACHE_PROFILE }`
		break
	elif [ $qps -lt $lowThresholdQ ] && [ $cacheInfo == "UPD_DNS_CACHE_PROFILE" ];then
		`tmsh modify ltm virtual ${arrVSL[$count]} profiles delete { UPD_DNS_CACHE_PROFILE } profiles add  { UPD_DNS_PROFILE }`
		break
	else
		((count++))
	fi
done

arrVSLTCP=(VS_UPD_DNS_53_10.168.0.0_TCP VS_UPD_DNS_53_10.172.0.0_TCP VS_UPD_DNS_53_10.176.0.0_TCP VS_UPD_DNS_53_10.180.0.0_TCP)
counTCP=0
while [ $counTCP -lt 3 ]
do
	cacheInfo=`tmsh list ltm virtual ${arrVSLTCP[$counTCP]} | grep PROFILE | awk '{ print $1 }'`
	if [ $qps -gt $highThresholdQ ] && [ $cacheInfo == "UPD_DNS_PROFILE_TCP" ];then
		`tmsh modify ltm virtual ${arrVSLTCP[$counTCP]} profiles delete { UPD_DNS_PROFILE_TCP } profiles add  { UPD_DNS_CACHE_PROFILE_TCP }`
		break
	elif [ $qps -lt $lowThresholdQ ] && [ $cacheInfo == "UPD_DNS_CACHE_PROFILE_TCP" ];then
		`tmsh modify ltm virtual ${arrVSLTCP[$counTCP]} profiles delete { UPD_DNS_CACHE_PROFILE_TCP } profiles add  { UPD_DNS_PROFILE_TCP }`
		break
	else
		((counTCP++))
	fi
done
