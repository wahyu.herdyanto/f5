#!/bin/sh
file="/shared/tmp/chassis_cpu_temp.txt"
if [ ! -f $file ];then
	touch $file

	for i in {1..15};do
		if [ $i -eq 1 ];then
			echo "Chassis Temperature" >> $file
		elif [ $i -eq 14 ];then
			echo "CPU Temperature" >> $file
		else
			echo '0' >> $file
		fi
	done
	echo "Last Checked : " >> $file
fi
i=1
while [ $i -le 15 ];do
	if [[ $i -ne 1 ]] && [[ $i -lt 14 ]];then
		prev_chas_temp=$(cat $file | sed -n $i'p')
		j=$(($i-1))
		curr_chas_temp=$(tmsh show sys hardware | grep -iA 13 "Chassis Temperature Status" | grep -iA 11 1 | awk '{print $3}' | sed -n $j'p' )
		echo "prev_$i: $prev_chas_temp"
		echo "curr_$i: $curr_chas_temp"
		if [ $curr_chas_temp -gt $prev_chas_temp ];then
			sed -i ""$i"s/$prev_chas_temp/$curr_chas_temp/" $file
		fi
	elif [ $i -eq 15 ];then
		prev_cpu_temp=$(cat $file | sed -n $i'p')
		curr_cpu_temp=$(tmsh show sys hardware | grep -iA 2 "CPU Status" | awk '{print $2}' | egrep '^[0-9]+$')
		echo "prev_$i: $prev_cpu_temp"
		echo "curr_$i: $curr_cpu_temp"
		if [ $curr_chas_temp -gt $prev_chas_temp ];then
			sed -i ""$i"s/$prev_cpu_temp/$curr_cpu_temp/" $file
		fi
	fi
	((i++))
done

datetime=$(date)
sed -i '/Last/c\'"Last Checked : ${datetime}"'' $file