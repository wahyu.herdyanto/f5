#!/bin/bash
i=1
sum=0
while [ $i -lt 97 ];do
        usage=$(tmsh show sys cpu | grep -w Util | awk '{print $8}' | sed -n "${i}p")
        sum=$(($sum + $usage))
        ((i++))
done
echo $(($sum / 96))
