#!/bin/sh
#mailto: wahyu.herdyanto@dimensiondata.com
#last update: 24-Sept-2016

#CHANGE-ME
totalBlades="3"

########################################################################
#ChangeThis IP
splunk_ip_address="10.1.1.x"

ssh_command="ssh -q -o StrictHostKeyChecking=no -l root $splunk_ip_address"
splunk_file="/tmp/bigip_check.txt"
local_file="/tmp/splunk.txt"
log_file="/var/log/ltm"
log_last_datetime="/tmp/log_last_datetime.txt"
blade_last_hour="/tmp/blade_last_hour.txt"
chassis_last_hour="/tmp/chassis_last_hour.txt"

#this interval is for traffic bps / connection collection, in seconds
interval=10

function check_cpu () {
	#we use sysMultiHostCpuUsageRatio5m (.1.3.6.1.4.1.3375.2.1.7.5.2.1.35) for collecting CPU usage of each core, then calculate manually.
	local sum=0
	numCore=$(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.7.5.2.1.35 | wc -l)
	for i in $(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.7.5.2.1.35 -Ov | awk '{print $2}');do
		sum=$(($sum + $i))
	done
	echo $(($sum / $numCore))%
}

function check_memory () {
	tmmMemUsage=$(snmpget -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.1.2.1.144.0 | awk '{print $4}')
	tmmMemTotal=$(snmpget -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.1.2.1.143.0 | awk '{print $4}')
	echo $(($tmmMemUsage * 100 / $tmmMemTotal))%
}

function check_blades () {
	chassisSlotNumber=$(snmpget -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.10.3.1.0 -Ov | awk '{print $2}')
	if [[ $chassisSlotNumber -eq 0 ]];then
		#it means the box is not modular, so we can skip this blade's checking and echo "OK"
		echo "OK"
	else
		#while 'x' is user-input for amount of physical being inserted into the chassis
		#and 'y' is the system-detected for amount of physical inserted into the chassis
		#this comparison below, just to make sure whenever new blade was inserted and the variable 'totalBlades' is accidentally misconfigured, the alert will not be sent.
		#however, when the user-input for 'totalBlades' is higher than system-detected, then alerting will be sent, indicates something wrong on certain blade(s).
		local x=$1
		local y=$(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.10.3.2.1.2 -Ov | awk '{print $2}' | sed '/^$/d' | wc -l)
		if [[ $x -ge $y ]];then
			tot_Blades=$x
		else
			tot_Blades=$y
		fi

		avail=$(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.10.2.2.1.3 -Ov | grep 'green' | wc -l)
		state=$(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.10.2.2.1.8 -Ov | grep 'running' | wc -l)
		lic=$(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.10.2.2.1.7 -Ov | grep 'true' | wc -l)
		ha=$(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.10.2.2.1.13 -Ov | egrep 'active|standby' | wc -l)
		reason=$(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.10.2.2.1.6 -Ov | grep 'Run' | wc -l)
		
		[ -e $blade_last_hour ] || touch $blade_last_hour
		local hour=$(date '+%H'|sed 's/^0*//')
		local last_hour=$(cat $blade_last_hour | sed 's/^0*//')
		if [[ $avail -ne $tot_Blades && $state -ne $tot_Blades && $lic -ne $tot_Blades && $ha -ne $tot_Blades && $reason -ne $tot_Blades ]] &&\
			[[ -z $blade_last_hour || $hour -ne $last_hour ]];then
			echo "NOK"
			echo $hour > $blade_last_hour
		else
			echo "OK"
		fi
	fi
}

function check_core () {
	local status="true"
	local year=$(date '+%Y'|sed 's/^0*//')
	local month=$(date '+%m'|sed 's/^0*//')
	local day=$(date '+%d'|sed 's/^0*//')
	local hour=$(date '+%H'|sed 's/^0*//')
	local minute=$(date '+%M'|sed 's/^0*//')
	
	core_file_lst=$(clsh "ls -lAh --time-style='+%Y-%m-%d %H:%M' /shared/core/" | egrep -v "total|slot")
	IFS=$'\n'
	for core_file in $core_file_lst;do
		core_year=`echo $core_file | awk '{print $6}' | cut -d \- -f1 | sed 's/^0*//'`
		core_month=`echo $core_file | awk '{print $6}' | cut -d \- -f2 | sed 's/^0*//'`
		core_day=`echo $core_file | awk '{print $6}' | cut -d \- -f3 | sed 's/^0*//'`
		core_hour=`echo $core_file | awk '{print $7}' | cut -d \: -f 1 | sed 's/^0*//'`
		core_minute=`echo $core_file | awk '{print $7}' | cut -d \: -f 2 | sed 's/^0*//'`
		if [[ $core_year -eq $year ]] && [[ $core_month -eq $month ]] && [[ $core_day -eq $day ]] &&\
			([[ $core_hour -eq $hour && $core_minute -ge $(($minute - 6)) ]] || \
			[[ $core_hour -eq $(($hour - 1)) && $core_minute -ge $(($minute - 6 + 60)) ]] || \
			[[ $hour -eq 0 && $core_hour -eq $(($hour - 1 + 24)) && $core_minute -ge $(($minute - 6 + 60)) ]]);then
			#note: actually just check core file in last 5 minutes, reason why we set 6s in calculation, because script's running time was taken into account.
			#hence there are only 1-2 alert SMS/email alert, if there's core produced.
			status="false"
		fi
		
	done
	unset IFS
	($status && echo "OK") || echo "NOK"
}

function check_crc () {
	local i=0
	local j=0
	for crc in $(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.2.4.4.3.1.8 -Ov | awk '{print $2}');do
		arrCrc1[$i]=$crc
		((i++))
	done
	sleep $interval
	i=0
	for crc in $(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.2.4.4.3.1.8 -Ov | awk '{print $2}');do
		arrCrc2[$i]=$crc
		((i++))
	done

	while [[ $j -le $i ]];do
		if [ ${arrCrc1[$j]} == ${arrCrc2[$j]} ];then
			((j++))
		else
			status="false"
			break
		fi
	done
	($status && echo "OK") || echo "NOK"
}

function check_chassis_temp () {
	temp_limit=40
	local status="true"
	platform_info=$(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.3.5.1 -Ov | awk '{print $2}')
	
	#if the system uses viprion 2200-blade2250, the system will always report 'OK', as the box lack of chassis temperature sensor.
	if [[ ! $platform_info == "A112" ]];then
		chassis_temp=$(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.3.2.3.2.1.2 -Ov | awk '{print $2}' | sed 's/^0*//')
		[ -e $chassis_last_hour ] || touch $chassis_last_hour
		local hour=$(date '+%H'|sed 's/^0*//')
		local last_hour=$(cat $chassis_last_hour | sed 's/^0*//')
		for temp in $(echo "$chassis_temp");do
			if [[ $temp -gt $temp_limit ]] && [[ -z $chassis_last_hour || $hour -ne $last_hour ]];then
				status="false"
				echo $hour > $chassis_last_hour
				break
			fi
		done
	fi
	($status && echo "OK") || echo "NOK"
}

function check_log () {
	local month=$(date '+%b'|sed 's/^0*//')
	local day=$(date '+%d'|sed 's/^0*//')
	local hour=$(date '+%H'|sed 's/^0*//')
	local minute=$(date '+%M'|sed 's/^0*//')
	
	[ -e $log_last_datetime ] || touch $log_last_datetime
	local last_datetime=`cat $log_last_datetime`
	if [[ -z $last_datetime || -z $(awk "/$last_datetime/,0" $log_file) ]];then
		#we start over searching suspicious things from beginning of the log, since the previous log had been rotated/archived.
		tail -n 1 $log_file | egrep -o "[a-zA-Z].+[0-9]+\:[0-9]+\:[0-9]+" > $log_last_datetime
		suspi_log=$(egrep -w "crit|alert|emerg" $log_file | awk -F'crit|alert|emerg' '{print $2}' | cut -d' ' -f4-7 | sort -u | tr -d '[:punct:]')
	else
		#we continue searching suspicious things on the same log file as before, start from last captured datetime
		tail -n 1 $log_file | egrep -o "[a-zA-Z].+[0-9]+\:[0-9]+\:[0-9]+" > $log_last_datetime
		suspi_log=$(awk "/$last_datetime/,0" $log_file | egrep -w "crit|alert|emerg" | awk -F'crit|alert|emerg' '{print $2}' | cut -d' ' -f4-7 | sort -u | tr -d '[:punct:]')
	fi
	
	log_month=$(cat "/tmp/log_last_datetime.txt" | awk '{print $1}')
	log_day=$(cat "/tmp/log_last_datetime.txt" | awk '{print $2}')
	log_hour=$(cat "/tmp/log_last_datetime.txt" | awk '{print $3}' | cut -d \: -f1 | sed 's/^0*//')
	log_minute=$(cat "/tmp/log_last_datetime.txt" | awk '{print $3}' | cut -d \: -f2 | sed 's/^0//')

	IFS=$'\n'
	#just in case there's suspicious things found on the last line of the log, but there's no additional entry in the log after 5 minutes, then the script will report OK.
	#this is because we only monitor log during last 5 minutes
	if [[ ! -z "$suspi_log" ]] && [[ $log_month -eq $month ]] && [[ $log_day -eq $day ]] &&\
		([[ $log_hour -eq $hour && $log_minute -ge $(($minute - 6)) ]] || \
		[[ $log_hour -eq $(($hour - 1)) && $log_minute -ge $(($minute - 6 + 60)) ]] || \
		[[ $hour -eq 0 && $log_hour -eq $(($hour - 1 + 24)) && $log_minute -ge $(($minute - 6 + 60)) ]]);then
		echo "$suspi_log"
	else
		echo "No suspicious things found in the last 5 minutes"
	fi
	unset IFS
}

function check_thp {
	local totBlades=$(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.10.3.2.1.2 -Ov | awk '{print $2}' | sed '/^$/d' | wc -l)
	box_capacity=$(($totBlades * 6700000000))
	#A : client_side_traffic.in
	#B : server_side_traffic.out
	#C : server_side_traffic.in
	#D : client_side_traffic.out
	
	init_clientBytesIn=$(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.1.2.1.3 | awk '{print $4}')
	init_clientBytesOut=$(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.1.2.1.5 | awk '{print $4}')
	init_serverBytesIn=$(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.1.2.1.10 | awk '{print $4}')
	init_serverBytesOut=$(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.1.2.1.12 | awk '{print $4}')
	sleep $interval
	last_clientBytesIn=$(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.1.2.1.3 | awk '{print $4}')
	last_clientBytesOut=$(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.1.2.1.5 | awk '{print $4}')
	last_serverBytesIn=$(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.1.2.1.10 | awk '{print $4}')
	last_serverBytesOut=$(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.1.2.1.12 | awk '{print $4}')

	deltaStatClientBytesIn=$(($last_clientBytesIn - $init_clientBytesIn))
	deltaStatClientBytesOut=$(($last_clientBytesOut - $init_clientBytesOut))
	deltaStatServerBytesIn=$(($last_serverBytesIn - $init_serverBytesIn))
	deltaStatServerBytesOut=$(($last_serverBytesOut - $init_serverBytesOut))
	
	####################################################################################
	# subs --A--> F5 --B--> Server
	# subs <--D-- F5 <--C-- Server
	
	#Definition : A
	clientBitsIn=$(($deltaStatClientBytesIn * 8 / $interval))
	
	#Definition : B
	serverBitsIn=$(($deltaStatServerBytesIn * 8 / $interval))
	
	#Definition : C
	serverBitsOut=$(($deltaStatServerBytesOut * 8 / $interval))

	#Definition : D
	clientBitsOut=$(($deltaStatClientBytesOut * 8 / $interval))
	
	#Definition : A + D (Total Client Side)
	totalClientBits=$(($clientBitsIn + $clientBitsOut))
	
	#Definition : B + C (Total Server Side)
	totalServerBits=$(($serverBitsIn + $serverBitsOut))
	
	#Definition : A + C (Total Inbound)
	#totalBitsIn=$(($clientBitsIn + $serverBitsOut))
	
	#Definition : B + D (Total Outbound)
	#totalBitsOut=$(($serverBitsIn + $clientBitsOut))
	####################################################################################
	
	#Sucess Rate : A to B (Subscriber Outbound)
	#SR_subsOutbound=$(($clientBitsIn * 100 / $serverBitsIn))%
	
	#Success Rate : C to D (Subscriber Inbound)
	#SR_subsInbound=$(($serverBitsOut * 100 / $clientBitsOut))%
	
	#Sucess Rate : A+D to B+C (Total Switch Client Side to Server Side)
	#SR_totalClient_Server=$(($totalClientBits *  100 / $totalServerBits))%
	
	#Success Rate : A+C to B+D (Total Switch Inbound to Outbound)
	#SR_totalInbound_Outbound=$(($totalBitsIn * 100 / $totalBitsOut))%
	
	#echo "$SR_subsOutbound|$SR_subsInbound|$SR_totalClient_Server|$SR_totalInbound_Outbound"
}

function check_newConn {
	#sysTcpStatAccepts1=$(snmpget -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.1.2.12.6.0 | awk '{print $4}')
	sysStatServerTotConns1=$(snmpget -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.1.2.1.14.0 | awk '{print $4}')
	sysStatClientTotConns1=$(snmpget -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.1.2.1.7.0 | awk '{print $4}')
	#sysTcpStatConnects1=$(snmpget -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.1.2.12.8.0 | awk '{print $4}')
	sleep $interval
	#sysTcpStatAccepts2=$(snmpget -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.1.2.12.6.0 | awk '{print $4}')
	sysStatServerTotConns2=$(snmpget -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.1.2.1.14.0 | awk '{print $4}')
	sysStatClientTotConns2=$(snmpget -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.1.2.1.7.0 | awk '{print $4}')
	#sysTcpStatConnects2=$(snmpget -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.1.2.12.8.0 | awk '{print $4}')
	
	#deltaTcpStatAccept=$(($sysTcpStatAccepts2 - $sysTcpStatAccepts1))
	deltaStatServerTotConns=$(($sysStatServerTotConns2 - sysStatServerTotConns1))
	deltaStatClientTotConns=$(($sysStatClientTotConns2 - sysStatClientTotConns1))
	#deltaTcpStatConnects=$(($sysTcpStatConnects2 - sysTcpStatConnects1))
	
	#New Connections Summary
	#newConnSum_ClientAccepts=$(($deltaTcpStatAccept / $interval))
	#newConnSum_ServerConnects=$(($deltaStatServerTotConns / $interval))
	
	#Total New Connections
	totNewConn_ClientConnects=$(($deltaStatClientTotConns / $interval))
	totNewConn_ServerConnects=$(($deltaStatServerTotConns / $interval))
	
	#New Accepts/Connects
	#newAcceptsConnects_ClientAccepts=$(($deltaTcpStatAccept / $interval))
	#newAcceptsConnects_ServerConnects=$(($deltaTcpStatConnects / $interval))

	#echo "newConnSum_ClientAccepts : $newConnSum_ClientAccepts"
	#echo "newConnSum_ServerConnects : $newConnSum_ServerConnects"
	#echo "totNewConn_ClientConnects : $totNewConn_ClientConnects"
	#echo "totNewConn_ServerConnects : $totNewConn_ServerConnects"
	#echo "newAcceptsConnects_ClientAccepts : $newAcceptsConnects_ClientAccepts"
	#echo "newAcceptsConnects_ServerConnects : $newAcceptsConnects_ServerConnects"
}

#failover_status is a state of the box whether ACTIVE or STANDBY
failover_status=$(snmpwalk -v2c -c F5_PDN localhost .1.3.6.1.4.1.3375.2.1.14.3.2 -Ov | awk '{print $2}')
hostname=$(tmsh list sys global-settings hostname | grep hostname | awk '{print $2}')
hostname_site=$(echo $hostname | cut -d '-' -f1)
if [[ $hostname_site == 'JPR' || $hostname_site == "TMK" ]];then
	hostname_nodeID=$(echo $hostname | cut -d '-' -f3)
else
	hostname_nodeID=$(echo $hostname | cut -d '-' -f4)
fi

finalName="F5-PDN-$hostname_site-$hostname_nodeID"
#echo "final-name : $finalName"

#run function check_thp & check_newConn, it will populate necessary variables
check_thp
check_newConn

#there's field below represents box-capacity. For the param which doesn't have box-capacity related information (e.g. CPU, memory, blades), the field will be NULL
echo "" > $local_file
echo "$finalName;$failover_status;NULL;\
`check_cpu`;NULL;\
`check_memory`;NULL;\
`check_blades $totalBlades`;NULL;\
`check_core`;NULL;\
`check_crc`;NULL;\
`check_log | perl -pe 'chomp if eof' | perl -0pe "s/\n/ \\| /g"`;NULL;\
$totalClientBits;$box_capacity;\
$totalServerBits;$box_capacity;\
$totNewConn_ClientConnects;NULL;\
$totNewConn_ServerConnects;NULL;$finalName" >> $local_file

cat $local_file | $ssh_command "cat >> $splunk_file"
