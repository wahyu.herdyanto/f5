#!/bin/sh
chsinfo=$(cat /var/tmp/chsinfo.txt)
#example of chsinfo.txt
#chs100286s
#chs100571s
#chs104431s
#chs104432s
#chs100260s
#chs100589s
#chs104433s
#chs104436s
#chs102542s

i=0
while [ $i -le 2 ]; do
	if [ $i -eq 0 ]; then
		echo "entiled-service-1:"
		for chs in $chsinfo; do
			j=$(grep -n $chs /var/tmp/prodinfo.txt | cut -d : -f1)
			if [ $j -ne 0 ]; then
				let "j += 6"
				#array+=($(sed -n "$j"p /var/tmp/prodinfo.txt))
				sed -n "$j"p /var/tmp/prodinfo.txt
			fi
		done
	fi
	echo "\n"
	(( i++ ))
	
	if [ $i -eq 1 ]; then
		echo "entiled-service-2:"
		for chs in $chsinfo; do
			j=$(grep -n $chs /var/tmp/prodinfo.txt | cut -d : -f1)
			if [ $j -ne 0 ]; then
				let "j += 7"
				sed -n "$j"p /var/tmp/prodinfo.txt
			fi
		done
	fi
	echo "\n"
	(( i++ ))
	
	if [ $i -eq 2 ]; then
		echo "warranty-end-date:"
		for chs in $chsinfo; do
			j=$(grep -n $chs /var/tmp/prodinfo.txt | cut -d : -f1)
			if [ $j -ne 0 ]; then
				let "j += 8"
				#array+=($(sed -n "$j"p /var/tmp/prodinfo.txt))
				sed -n "$j"p /var/tmp/prodinfo.txt
			fi
		done
	fi
	echo "\n"
	(( i++ ))
done