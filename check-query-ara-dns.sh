#!/bin/sh
###################################
#wahyu.herdyanto@dimensiondata.com#
###################################

qname_roaming_3G="internet.mnc001.mcc514.gprs"
qname_roaming_4G="internet.apn.epc.mnc001.mcc514.3gppnetwork.org"

qname_3G_e="internet.8111792.mnc010.mcc510.gprs"
qname_4G_e="internet.8111792.apn.epc.mnc010.mcc510.3gppnetwork.org"

qname_3G_h="internet.8533792.mnc010.mcc510.gprs"
qname_4G_h="internet.8533792.apn.epc.mnc010.mcc510.3gppnetwork.org"

qname_CSFB="rac0002.lac17BC.mnc0010.mcc0510.gprs"

rd="7"
QNAMES=($qname_roaming_3G $qname_roaming_4G $qname_3G_e $qname_4G_e $qname_3G_h $qname_4G_h $qname_CSFB)
VSL=$(tmsh list ltm virtual VS_ARA_DNS_* | grep "ltm virtual" | awk '{print $3}#')

for VS in $VSL; do
	echo "*******THE FOLLOWING IS DIG RESULT FOR VS $VS********"
	IP=$(tmsh list ltm virtual $VS | grep -i destination | awk '{print $2}' | cut -d % -f1)
	VL=$(tmsh list ltm virtual $VS | grep -iA 1 "vlans {" | sed -n 2p)
	if [ ! -z "$VL" ]; then
		SRC_IP=$(tmsh list net self | grep -iB 3 $VL | sed -n 1p | awk '{print $3}' | cut -d '%' -f1)
		if [ ! -z "$SRC_IP" ]; then
			i=0
			while [ "x${QNAMES[$i]}" != "x" ]; do
				#echo "SRC_IP:$SRC_IP & IP: $IP"
				if [ $i -eq 0 ]; then
					echo "###This is query for qname_roaming_3G###"
					rdexec $rd dig +noauthority +nostats -b $SRC_IP @$IP ${QNAMES[$i]}
				elif [ $i -eq 1 ]; then
					echo "###This is query for qname_roaming_4G###"
					rdexec $rd dig +noauthority +nostats -b $SRC_IP @$IP ${QNAMES[$i]} -t NAPTR
				elif [ $i -eq 2 ]; then
					echo "###This is query for qname_3G_ericsson###"
					rdexec $rd dig +noauthority +nostats -b $SRC_IP @$IP ${QNAMES[$i]}
				elif [ $i -eq 3 ]; then
					echo "###This is query for qname_4G_ericsson###"
					rdexec $rd dig +noauthority +nostats -b $SRC_IP @$IP ${QNAMES[$i]} -t NAPTR
				elif [ $i -eq 4 ]; then
					echo "###This is query for qname_3G_huawei###"
					rdexec $rd dig +noauthority +nostats -b $SRC_IP @$IP ${QNAMES[$i]}
				elif [ $i -eq 5 ]; then
					echo "###This is query for qname_4G_huawei###"
					rdexec $rd dig +noauthority +nostats -b $SRC_IP @$IP ${QNAMES[$i]} -t NAPTR
				elif [ $i -eq 6 ]; then
					echo "###This is query for qname_CSFB###"
					rdexec $rd dig +noauthority +nostats -b $SRC_IP @$IP ${QNAMES[$i]}
				fi  
				
				i=$(( $i+1 ))
			done
		fi

	fi
	echo "#########################################################################################"
done
