#!/bin/bash
#paramLst = { destination_addr destination_port iRules Pool NAT-Type NATpool Persistence profiles }
vsLst=tmsh list ltm virtual | grep "ltm virtual" | awk '{print $3}'
paramLst="destination rules pool source-address-translation persist profiles"

if [ ! -e data.csv ];then
	$(> data.csv)
fi

for i in $vsLst;do
	cmd="tmsh list ltm virtual $i"
	arrData[0]=$i
	for j in $paramLst;do
		if [ $(eval $cmd | grep -n $j | awk '{print $2}')  == "destination" ];then
			dst_addr=$(eval $cmd | grep destination | awk '{print $2}' | cut -d : -f1)
			dst_port=$(eval $cmd | grep destination | awk '{print $2}' | cut -d : -f2)
		
		elif [ $(eval $cmd | grep -n $j | awk '{print $2}')  == "rules" ];then
			idx=$(eval $cmd | grep -n $j | awk '{print $1}' | cut -d : -f1)
			rules=""
			while [ $(eval $cmd | sed -n '$idx'p) != "    }" ];do
				rules += $(eval $cmd | sed -n '$idx'p | awk '{print $1}')
				((idx++))
			done
			
		elif [ $(eval $cmd | grep -n $j | awk '{print $2}')  == "pool" ];then
			pool=$(eval $cmd | grep destination | awk '{print $2}')
		#pool jangan ketuker dengan pool snat
		elif [ $(eval $cmd | grep -n $j | awk '{print $2}')  == "profiles" ];then
			idx=$(eval $cmd | grep -n $j | awk '{print $1}' | cut -d : -f1)
			profiles=""
			while [ $(eval $cmd | sed -n '$idx'p) != "    }" ];do
				profiles += $(eval $cmd | sed -n '$idx'p | awk '{print $1}')
				((idx++))
			done	