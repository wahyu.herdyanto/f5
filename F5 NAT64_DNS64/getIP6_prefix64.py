#!/usr/bin/python3.6
import sys

def getip6_prefix64(ip6_addr):
	if "::" in ip6_addr:
		ip6_left = ip6_addr.split("::")[0]
		ip6_right = ip6_addr.split("::")[1]
		len_init = len(ip6_left.split(":")) + len(ip6_right.split(":"))
		len_zero = 8 - len_init
		i = 0
		new_delim = ""
		while i < len_zero:
			new_delim += ":0"
			i += 1
		new_delim += ":"
		
		ip6_full = ip6_left + new_delim + ip6_right
		#print(ip6_full)
		
	else:
		ip6_full = ip6_addr
		#print(ip6_full)


	i=0
	ip6_prefix64 = ""
	while i < 4:
		ip6_prefix64 += ip6_full.split(":")[i] + ":"
		i += 1

	ip6_prefix64 = ip6_prefix64.rstrip(":")
	
	return ip6_prefix64
	
print(getip6_prefix64(sys.argv[1]))


#ip6_addr = "2204:c0:ffff:fff0:1234:0:4567:132"
#ip6_addr = "2204:c0:ffff:fff0::132"

