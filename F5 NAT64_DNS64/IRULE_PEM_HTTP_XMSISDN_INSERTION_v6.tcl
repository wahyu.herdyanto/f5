#This sample iRule is for header enrichment purpose
#Radius attribute's value is mapped to PEM

ltm rule IRULE_PEM_HEADER_ENRICHMENT_IP6 {
when RULE_INIT {
	set static::xmsisdn_debug 0
}

when HTTP_REQUEST {
#Please replace data group name with the proper one
    if { [class match -- [string tolower [HTTP::host][HTTP::uri]] starts_with $DATAGROUP] or \
    [class match -- [string tolower [HTTP::uri]] contains DG_XMSISDN_URI_TEST_v6] } {
        log local0. "IPv6 ADDRESS : [IP::client_addr]"
		set ip6_addr [IP::client_addr]
		if { [string match "*::*" [IP::client_addr]] } {
			set ip6_left [getfield $ip6_addr "::" 1]
			set ip6_right [getfield $ip6_addr "::" 2]
			set len_init [expr {[llength [split $ip6_left ":"]] + [llength [split $ip6_right ":"]]}]
			set len_zero [expr {8 - $len_init}]
			set i 0
			set new_delim ""
			while { $i < $len_zero } {
				#set new_delim [concat $new_delim":0"]
				append new_delim ":0"
				incr i
			}
			append new_delim ":"
			
			set ip6_full [concat $ip6_left$new_delim$ip6_right]
			
		} else {
			set ip6_full $ip6_addr
		}
		
		log local0. "IP6_FULL : $ip6_full"
		
		set i 0
		set ip6_prefix64 ""
		while { $i < 4 } {
			append ip6_prefix64 [lindex [split $ip6_full ":"] $i]:
			incr i
		}

		append ip6_prefix64 :
		log local0. "IP6_PREFIX64 : $ip6_prefix64"
		
        set msisdn [PEM::session info $ip6_prefix64 calling-station-id]
        HTTP::header insert MSISDN $msisdn
        if { $static::xmsisdn_debug } {
            # local0. "IP Address [IP::client_addr] has MSISDN : $msisdn"
        }
    }
}
}
